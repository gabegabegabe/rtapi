const DEFAULT_ALLOWED_ORIGINS = 'http://localhost:1234';
const DEFAULT_PORT = 3000;

export const ALLOWED_ORIGINS = process.env.CORS_ORIGINS ?? DEFAULT_ALLOWED_ORIGINS;
export const PORT = process.env.PORT ?? DEFAULT_PORT;
