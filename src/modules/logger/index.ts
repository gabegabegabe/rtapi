import consola from 'consola';
import { Logger } from '~/models/logger.model';

const logger = new Logger(consola);

export default logger;
