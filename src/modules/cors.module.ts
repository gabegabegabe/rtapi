import { ALLOWED_ORIGINS } from '~/globals/environment.globals';
import type { Context } from 'koa';
import type { Immutable } from '~/models/immutable.model';
import koaCors from '@koa/cors';

const validOrigins = ALLOWED_ORIGINS.split(',');

const originIsValid = (origin: string): boolean => validOrigins
	.includes(origin);

const verifyOrigin = ({ headers: { origin } }: Immutable<Context>): string => {
	if (typeof origin !== 'string') return '';

	if (originIsValid(origin)) return origin;

	return '';
};

export const cors = koaCors({
	allowMethods: ['GET'],
	origin: verifyOrigin
});
