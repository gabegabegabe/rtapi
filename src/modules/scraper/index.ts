import logger from '~/modules/logger';
import { RTScraper } from '@rt_lite/scraper';

const scraper = new RTScraper(true, logger);

export default scraper;
