import type { Context } from 'koa';
import logger from '~/modules/logger';
import Router from '@koa/router';
import {
	frontPage,
	search,
	searchMovies,
	searchTV
} from '~/services/rt.service';
import type {
	MediaSet,
	Movie,
	SearchResults,
	TVSeries
} from '@rt_lite/common/models';

type SearchParams = {
	terms: string;
};

const NO_RESULTS: SearchResults = {
	movies: [],
	tvSeries: []
};

const router = new Router();

// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
router.get('/', async (ctx: Context) => {
	let sets: MediaSet[] = [];

	try {
		sets = await frontPage();
	} catch (err) {
		logger.error(err);
	} finally {
		ctx.body = sets;
	}
});

// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
router.get('/:terms', async (ctx: Context) => {
	const { terms } = ctx.params as SearchParams;

	let results = { ...NO_RESULTS };

	try {
		results = await search(terms);
	} catch (err) {
		logger.error(err);
	} finally {
		// eslint-disable-next-line require-atomic-updates
		ctx.body = results;
	}
});

// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
router.get('/movies/:terms', async (ctx: Context) => {
	const { terms } = ctx.params as SearchParams;

	let results: Movie[] = [];

	try {
		results = await searchMovies(terms);
	} catch (err) {
		logger.error(err);
	} finally {
		// eslint-disable-next-line require-atomic-updates
		ctx.body = results;
	}
});

// eslint-disable-next-line @typescript-eslint/prefer-readonly-parameter-types
router.get('/tv-series/:terms', async (ctx: Context) => {
	const { terms } = ctx.params as SearchParams;

	let results: TVSeries[] = [];

	try {
		results = await searchTV(terms);
	} catch (err) {
		logger.error(err);
	} finally {
		// eslint-disable-next-line require-atomic-updates
		ctx.body = results;
	}
});

export default router;
