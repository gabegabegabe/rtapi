const MS_IN_S = 1000;
const S_IN_M = 60;
const M_IN_H = 60;
const H_IN_D = 24;
const D_IN_W = 7;
const W_IN_Y = 52;

export const time = (ms: number): {
	milliseconds: number;
	seconds: number;
	minutes: number;
	hours: number;
	days: number;
	weeks: number;
	years: number;
} => ({
	/* eslint-disable sort-keys */
	get milliseconds (): number { return ms; },
	get seconds (): number { return this.milliseconds * MS_IN_S; },
	get minutes (): number { return this.seconds * S_IN_M; },
	get hours (): number { return this.minutes * M_IN_H; },
	get days (): number { return this.hours * H_IN_D; },
	get weeks (): number { return this.days * D_IN_W; },
	get years (): number { return this.weeks * W_IN_Y; }
	/* eslint-enable sort-keys */
});
