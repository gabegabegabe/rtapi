import { cors } from '~/modules/cors.module';
import Koa from 'koa';
import logger from '~/modules/logger';
import { PORT } from '~/globals/environment.globals';
import router from '~/modules/router';

const app = new Koa();

app
	.use(cors)
	.use(router.routes())
	.use(router.allowedMethods());

app.listen(PORT, () => {
	logger.success(`App listening on port ${PORT}`);
});
