export type ICacheRecord <T> = {
	contents: T;
	readonly created: Date;
	readonly modified: Date;
	readonly read: Date;
};

export class CacheRecord <T> implements ICacheRecord<T> {
	#contents: T;
	#created = Date.now();
	#modified = this.#created;
	#read = this.#created;

	constructor (contents: T) {
		this.#contents = contents;
	}

	get created (): Date {
		return new Date(this.#created);
	}

	get modified (): Date {
		return new Date(this.#modified);
	}

	get read (): Date {
		return new Date(this.#read);
	}

	get contents (): T {
		this.#read = Date.now();

		return this.#contents;
	}

	set contents (contents: T) {
		this.#modified = Date.now();
		this.#read = this.#modified;
		this.#contents = contents;
	}
}
