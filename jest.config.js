module.exports = {
	collectCoverage: false,
	collectCoverageFrom: ['./src/**'],
	coveragePathIgnorePatterns: ['/node_modules/'],
	coverageThreshold: {
		global: { lines: 90 }
	},
	moduleFileExtensions: [
		'js',
		'json',
		'ts'
	],
	moduleNameMapper: {
		'^~/(?<path>.*)': '<rootDir>/src/$1'
	},
	preset: 'ts-jest',
	roots: ['<rootDir>/src/'],
	testEnvironment: 'node'
};
