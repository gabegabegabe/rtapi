const { build } = require('esbuild');
const pkg = require('./package.json');

const FLAGS = {
	watch: '--watch'
};

// eslint-disable-next-line no-magic-numbers
const args = process.argv.slice(2);

const entryPoints = ['./src/index.ts'];
const external = Object.keys(pkg.dependencies);
const outdir = './dist';

const watchHandler = {
	onRebuild: (error, result) => {
		if (error) console.error('Watch build failed:', error);
		else console.info('Watch build succeeded:', result);
	}
};

const main = async () => {
	const start = Date.now();

	try {
		await build({
			bundle: true,
			entryPoints,
			external,
			format: 'cjs',
			minify: true,
			outdir,
			platform: 'node',
			sourcemap: false,
			watch: args.includes(FLAGS.watch) ? watchHandler : false
		});

		console.info(`Build completed in ${Date.now() - start}ms`);
	} catch (err) {
		console.error(err);

		process.exit(1);
	}
};

main();
